
package JavaInterface;

public class PhoneUser{
   Phone phone;

    public PhoneUser(Phone phone) {
        this.phone = phone;
    }

   void turnOnThePhone(){
       this.phone.PowerOn();
   }
   
   void turnOffThePhone(){
       this.phone.PowerOff();
   }
   
   void makePhoneLouder(){
       this.phone.VolumeUp();
   }
   
   void makePhoneSilent(){
       this.phone.VolumeDown();
   }
}
