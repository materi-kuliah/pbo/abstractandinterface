
package JavaInterface;

public interface Phone {
    int MAX_VOLUME = 100;
    int MIN_VOLUME = 0;
    
    void PowerOn();
    void PowerOff();
    void VolumeUp();
    void VolumeDown();
}
