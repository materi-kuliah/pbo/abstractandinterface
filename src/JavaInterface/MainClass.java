/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaInterface;

import java.util.Scanner;

/**
 *
 * @author mymac
 */
public class MainClass {
    
    public static void main(String[] args) {
    
        //Membuat Objek Hp
        Xiaomi xiaomi = new Xiaomi();
        
        //Membuat Objek User
        PhoneUser aji = new PhoneUser(xiaomi);
        
        //nyalakan hp
        aji.turnOnThePhone();
        
        //buat input untuk mempermudah percobaan
        Scanner input = new Scanner(System.in);
        String aksi;
        
        while(true){
            System.out.println("=== APLIKASI INTERFACE ===");
            System.out.println("[1] Nyalakan HP");
            System.out.println("[2] Matikan HP");
            System.out.println("[3] Perbesar Volume");
            System.out.println("[4] Kecilkan Volume");
            System.out.println("[0] Keluar");
            System.out.println("--------------------------");
            System.out.print("Pilih aksi> ");
            aksi = input.nextLine();
            
            if(aksi.equalsIgnoreCase("1")){
                aji.turnOnThePhone();
            } else if (aksi.equalsIgnoreCase("2")){
                aji.turnOffThePhone();
            } else if (aksi.equalsIgnoreCase("3")){
                aji.makePhoneLouder();
            } else if (aksi.equalsIgnoreCase("4")){
                aji.makePhoneSilent();
            } else if (aksi.equalsIgnoreCase("0")){
                System.exit(0);
            } else {
                System.out.println("Kamu memilih aksi yang salah!");
            }
        }
        
    }
}
