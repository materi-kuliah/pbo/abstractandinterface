package JavaInterface;

public class Xiaomi implements Phone{

    int volume;
    boolean isPowerOn;
    
    Xiaomi(){
        this.volume = 50;
    }
    
    @Override
    public void PowerOn() {
        isPowerOn = true;
        System.out.println("Handphone menyala....");
        System.out.println("Selamat datang di Xiaomi");
        System.out.println("Android Versi 29");
    }

    @Override
    public void PowerOff() {
        isPowerOn = false;
        System.out.println("Handphone dimatikan");
    }

    @Override
    public void VolumeUp() {
        if(isPowerOn){
           if(this.volume==MAX_VOLUME){
               System.out.println("Volume Full");
               System.out.println("sudah "+this.getVolume() +"%");
           }else{
               this.volume +=10;
               System.out.println("Volume Sekarang "+ this.getVolume() +"%");
           }
        }else{
            System.out.println("Nyalakan dulu dong hp nya");
        }
    }

    @Override
    public void VolumeDown() {
        if(isPowerOn){
            if(this.volume == MIN_VOLUME){
                System.out.println("Volume sudah 0%");
            }else{
                this.volume -=10;
                System.out.println("Volume Sekarang "+ this.getVolume() +"%");
            }
        }else{
            System.out.println("Nyalakan dulu dong hp nya");
        }
    }
    
    
   public int getVolume(){
       return this.volume;
   }
}
