
package abstractandinterface;

public class AbstractAndInterface {

    public static void main(String[] args) {
        Triangle triangle = new Triangle(6, 9);
        triangle.setColor("Merah");
        
        Circle circle = new Circle(6);
        circle.setColor("Biru");
        
        System.out.println("Data Keluaran dari Segitiga");
        System.out.println("Warna segigita adalah :"+ triangle.getColor());
        System.out.println("Luas Segitiga adalah :"+ triangle.getArea());
        
        System.out.println("\n");
        System.out.println("----------------------------");
        System.out.println("Data Keluaran dari Lingkaran");
        System.out.println("Warna lingkarang adalah :"+circle.getColor());
        System.out.println("Luas Lingkaran adalah :"+ circle.getArea());
    }
    
}
