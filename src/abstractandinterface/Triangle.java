
package abstractandinterface;

public class Triangle extends Shape{
    
    float alas;
    float tinggi;
    
    Triangle(float alas, float tinggi){
        this.alas = alas;
        this.tinggi = tinggi;
    }
    
    @Override
    float getArea() {
        return 0.5f * alas * tinggi;
    }
    
}
